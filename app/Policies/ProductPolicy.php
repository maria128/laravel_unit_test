<?php

namespace App\Policies;

use App\Entities\Product;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can delete the product.
     *
     * @param  \App\User  $user
     * @param  \App\Entities\Product  $product
     *
     * @return mixed
     */
    public function deleteProduct(User $user, Product $product)
    {
        return $user->id == $product->user_id;
    }
    public function delete(User $user, Product $product)
    {
        return $user->id == $product->user_id;
    }
}
