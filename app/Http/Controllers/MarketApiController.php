<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use Illuminate\Http\Request;
use App\Services\MarketService;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;

class MarketApiController extends Controller
{
    private $marketService;

    public function __construct(MarketService $marketService)
    {
        $this->marketService = $marketService;
    }

    public function showList()
    {
        $products = $this->marketService->getProductList();

        return $products->map(function ($product) {
            return new ProductResource($product);
        });
    }

    public function store(Request $request)
    {
        try {
            $request->request->add(['product_name' => $request->input('name')]);
            $request->request->add(['product_price' => $request->input('price')]);
            $product = $this->marketService->storeProduct($request);


            return new Response($product, 200);
        } catch (ValidationException $e) {
            return response()->json(['error' => $e->errors()], 400);
        } catch (\LogicException $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        } catch (\Exception $e) {
            return response()->json(['error' => 'something went wrong'], 400);
        }
    }

    public function showProduct(int $id)
    {
        try {
            return new ProductResource($this->marketService->getProductById($id));
        } catch (ValidationException $e) {
            return response()->json(['error' => $e->errors()], 400);
        } catch (\LogicException $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        } catch (\Exception $e) {
            return response()->json(['error' => 'something went wrong'], 400);
        }
    }

    public function delete(Request $request)
    {
        try {
            $this->marketService->deleteProduct($request);

            return new Response($request->id);
        } catch (ValidationException $e) {
            return response()->json(['error' => $e->errors()], 400);
        } catch (\LogicException $e) {
            return response()->json(['error' => $e->getMessage()], 400);
        } catch (\Exception $e) {
            return response()->json(['error' => 'something went wrong'], 400);
        }
    }
}
