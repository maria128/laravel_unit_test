<?php

namespace Tests\Unit\Service;

use App\Entities\Product;
use App\Repositories\ProductRepository;
use App\Services\MarketService;
use Illuminate\Database\Eloquent\Collection;
use Tests\TestCase;

class MarketServiceTest extends TestCase
{
    protected $marketService;

    protected function setUp(): void
    {
        parent::setUp();
        $marketServiceStub = $this->createMock(ProductRepository::class);

        $marketServiceStub->method('findAll')
            ->willReturn(
                new Collection([
                    factory(Product::class)->make([
                        'id' => 1,
                        'name' => 'Test Product 3',
                        'price' => '199.00',
                        'user_id' => 2,
                    ]),
                    factory(Product::class)->make([
                        'id' => 2,
                        'name' => 'Test Product 2',
                        'price' => '99.00',
                        'user_id' => 3,
                    ]),
                    factory(Product::class)->make([
                        'id' => 3,
                        'name' => 'Test Product 1',
                        'price' => '00.00',
                        'user_id' => 1,
                    ]),
                ])
            );
        $marketServiceStub->method('findById')
            ->willReturn(
                factory(Product::class)->make([
                        'id' => 1,
                        'name' => 'Test Product 3',
                        'price' => '199.00',
                        'user_id' => 2,
                    ])
            );

        $marketServiceStub->method('findByUserId')
            ->willReturn(
                new Collection([
                factory(Product::class)->make([
                    'id' => 3,
                    'name' => 'Test Product 1',
                    'price' => '00.00',
                    'user_id' => 1,
                ]),
                        ])
            );
        $marketServiceStub->method('store')
            ->willReturn(
                factory(Product::class)->make([
                        'id' => 2,
                        'name' => 'Test Product 2',
                        'price' => '50.13',
                        'user_id' => 1,
                ])
            );

        $this->marketService = new MarketService($marketServiceStub);
    }


    public function test_product_list(): void
    {
        $products = $this->marketService->getProductList();
        $this->assertCount(3, $products, 'Did not get correct count for all()');
        $this->assertCount(3, $products, 'Did not get correct count for all()');
        $this->assertProductsData($products);
    }

    public function test_product_by_id(): void
    {
        $product = $this->marketService->getProductById(1);

        $this->assertEquals(1, $product->id, 'No correct id');
        $this->assertEquals('Test Product 3', $product->name, 'No correct name');
        $this->assertEquals(199.00, $product->price, 'No correct price');
        $this->assertIsFloat(199.00, $product->price);
        $this->assertEquals(2, $product->user_id, 'No correct user id');
    }

    public function test_product_find_by_user_id()
    {
        $product = $this->marketService->getProductsByUserId(1);
        $this->assertProduct($product);
    }

    public function test_store_product()
    {
        $requestParams = [
            'name' => 'Test Product 2',
            'price' => '50.13',
            'user_id' => 1,
        ];

        $request = $this->getMockBuilder('Illuminate\Http\Request')
            ->disableOriginalConstructor()
            ->setMethods(['input'])
            ->getMock();
        $request->expects($this->any())
            ->method('input')
            ->willReturn($requestParams);
        $product = $this->marketService->storeProduct($request);

        $this->assertEquals(2, $product->id, 'No correct id');
        $this->assertEquals('Test Product 2', $product->name, 'No correct name');
        $this->assertEquals(50.13, $product->price, 'No correct price');
        $this->assertIsFloat(50.13, $product->price);
        $this->assertEquals(1, $product->user_id, 'No correct user id');
    }

    private function assertProductsData($products): void
    {
        foreach ($products as $product) {
            $this->assertInstanceOf(Product::class, $product);
            $this->assertNotEmpty($product->id);
            $this->assertNotEmpty($product->name);
            $this->assertNotEmpty($product->price);
            $this->assertNotEmpty($product->user_id);
        }
    }

    private function assertProduct($product): void
    {
        foreach ($product as $item) {
            $this->assertEquals(3, $item->id, 'No correct id');
            $this->assertEquals('Test Product 1', $item->name, 'No correct name');
            $this->assertEquals(00.00, $item->price, 'No correct price');
            $this->assertIsFloat(00.00, $item->price);
            $this->assertEquals(1, $item->user_id, 'No correct user id');

            $this->assertInstanceOf(Product::class, $item);
            $this->assertNotEmpty($item->id);
            $this->assertNotEmpty($item->name);
            $this->assertNotEmpty($item->price);
            $this->assertNotEmpty($item->user_id);
        }
    }
}
