<?php

namespace Tests\Feature;

use App\Entities\Product;
use App\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MarketApiServiceTest extends TestCase
{
    use WithFaker;

    private $attributes = [];

    public function setUp(): void
    {
        parent::setUp();
    }

    function test_show_list()
    {
        $response = $this->get("api/items/");
        $this->assertEquals(200, $response->getStatusCode());
    }


    function test_store()
    {
        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("POST", "api/items/", [
            'name' => "Test Product 3",
            'price' => "99.13",
            'user_id' => 1
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseHas("products", [
            'name' => "Test Product 3",
            'price' => "99.13",
            'user_id' => 1
        ]);
    }

    public function test_show_product()
    {
        $product = factory(Product::class)->create([
            'name' => "Test Product 2",
            'price' => "60.13",
            'user_id' => 1
        ]);
        $productId = $product->toArray()['id'];

        $response = $this->json("GET", "api/items/". $productId);

        $response->assertStatus(200);
        $this->assertDatabaseHas("products", [
            'name' => "Test Product 2",
            'price' => "60.13",
            'user_id' => 1
        ]);

        $response = $this->json("GET", "api/items/". 0);
        $response->assertStatus(400);
    }

    public function test_delete_product()
    {
        $product = factory(Product::class)->create([
            'name' => "Test Product 1",
            'price' => "11.00",
            'user_id' => 1
        ]);
        $productId = $product->toArray()['id'];

        $response = $this->json("DELETE", "api/items/". $productId);
        $response->assertStatus(401);

        $user = factory(User::class)->create();
        $response = $this->actingAs($user)->json("DELETE", "api/items/". $productId);
        $response->assertStatus(200);
        $response->assertJsonFragment([$productId]);
    }
}
