<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Entities\Product;
use App\User;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->text($maxNbChars = 10),
        'price' => $faker->numberBetween(100, 10000),
        'user_id' => User::query()->inRandomOrder()->first()->id,
    ];
});
